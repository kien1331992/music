package com.kien.musiclistent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kien.musiclistent.R;
import com.kien.musiclistent.model.Song;

import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<
        SongAdapter.DetailsViewHolder> {
    List<Song> lvSong;
    Context context;
    LayoutInflater inflater;
    private ItemClickListener mClickListener;
    private int postionChoice;
    private boolean defaultChoice;

    public SongAdapter(Context ct, List<Song> _lvSong) {
        this.context = ct;
        this.lvSong = _lvSong;
        inflater = LayoutInflater.from(this.context);
        defaultChoice = true;
    }

    @Override
    public DetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_song, parent, false);
        return new DetailsViewHolder(view);
    }

    public void positionChoice(int position) {
        postionChoice = position;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(DetailsViewHolder holder, int position) {
        if (position == 0 && defaultChoice) {
            defaultChoice = false;
            mClickListener.onPlayDefaultSong();
        }
        Song song = lvSong.get(position);
        holder.tvName.setText(song.getName());
        holder.tvIndex.setText(position + 1 + "");
        holder.tvTimeSong.setText(song.getTime());
        if (position == lvSong.size() - 1) {
            holder.view.setVisibility(View.GONE);
        }
        if (position == postionChoice) {
            holder.rlParent.setBackgroundColor(context.getResources().getColor(R.color._3b3849));
        } else {
            holder.rlParent.setBackgroundColor(context.getResources().getColor(R.color
                    .colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return lvSong.size();
    }

    public class DetailsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvIndex;
        TextView tvName;
        TextView tvTimeSong;
        View view;
        RelativeLayout rlParent;

        public DetailsViewHolder(View itemView) {
            super(itemView);
            tvIndex = itemView.findViewById(R.id.tvIndex);
            tvName = itemView.findViewById(R.id.tvName);
            tvTimeSong = itemView.findViewById(R.id.tvTimeSong);
            view = itemView.findViewById(R.id.view);
            rlParent = itemView.findViewById(R.id.rlParent);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);

        void onPlayDefaultSong();
    }
}

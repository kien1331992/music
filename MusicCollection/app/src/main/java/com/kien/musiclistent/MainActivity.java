package com.kien.musiclistent;

import android.Manifest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.kien.musiclistent.Database.SQLiteDAO;
import com.kien.musiclistent.adapter.SongAdapter;
import com.kien.musiclistent.controller.MusicController;
import com.kien.musiclistent.model.Song;
import com.kien.musiclistent.utils.Const;
import com.kien.musiclistent.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements SongAdapter.ItemClickListener,
        MusicController.SeekBarDelegate {
    SharedPrefs sp;
    List<Song> arrSongs = new ArrayList<>();

    @BindView(R.id.rvSongs)
    RecyclerView rvSongs;
    @BindView(R.id.seekbar)
    SeekBar seekbar;
    @BindView(R.id.tvCountSong)
    TextView tvCountSong;
    @BindView(R.id.icPause)
    ImageView icPause;
    @BindView(R.id.imvLoop)
    ImageView imvLoop;

    private SongAdapter sAdapter;
    private MusicController controller;
    private MediaPlayer mediaPlayer;
    private Handler mHandler = new Handler();
    private int count = 0;
    private boolean isRepeat = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        sp = SharedPrefs.getInstance();
        controller = new MusicController(this);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        controller.setMediaPlayer(mediaPlayer, this);

        checkPermission();
        seekbar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seekbar));
        initFinishMusic();
        initSeekBar();

    }

    private void initSeekBar() {
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mediaPlayer != null && fromUser){
                    mediaPlayer.seekTo(progress);
                }
            }
        });
    }

    private void initFinishMusic() {
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (!isRepeat)
                {
                    if (count >= arrSongs.size()-1)
                    {
                        count = 0;
                    } else {
                        count = count + 1;
                    }
                }
                sAdapter.positionChoice(count);
                onItemClick(count);
            }
        });
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            Dexter.withActivity(MainActivity.this)
                    .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                // do you work now
                                checkInstallDb();
                            } else {
                                finish();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest>
                                                                               permissions,
                                                                       PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .check();
        } else {
            checkInstallDb();
        }
    }

    private void initRecycleView() {
        sAdapter = new SongAdapter(this, arrSongs);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvSongs.setLayoutManager(manager);
        sAdapter.setClickListener(this);
        rvSongs.setAdapter(sAdapter);
    }

    private void loadDb() {
        arrSongs.addAll(SQLiteDAO.createQuery(Song.class).perform());
        tvCountSong.setText(arrSongs.size() + " bài hát");
        initRecycleView();
    }

    private void checkInstallDb() {
        String username = sp.get(Const.KEY_INSTALL_APP, String.class);
        if ("".equals(username)) {
            SharedPrefs.getInstance().put(Const.KEY_INSTALL_APP, "");
            createDb();
        }
        loadDb();
    }

    private void createDb() {
        SQLiteDAO.initialize(this);
        if (!SQLiteDAO.checkDB()) {
            SQLiteDAO.createDB();
        }
    }

    @OnClick(R.id.icPause)
    public void PauseResume() {
        if (mediaPlayer.isPlaying()) {
            icPause.setImageDrawable(getResources().getDrawable(R.drawable.ic_resume));
            controller.stopSong();
        } else {
            icPause.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
            controller.resume();
        }
    }

    @OnClick(R.id.imvLoop)
    public void repeatSong() {
        isRepeat = !isRepeat;
        if (isRepeat)
            imvLoop.setImageDrawable(getResources().getDrawable(R.drawable.icon_loop_choose));
        else
            imvLoop.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon));
    }

    @Override
    public void onItemClick(int position) {
        sAdapter.positionChoice(position);
        count = position;
        Song song = arrSongs.get(position);
        String filename = "android.resource://" + this.getPackageName() + "/raw/" + song.getUri();
        controller.playSong(filename);
    }

    @Override
    public void onPlayDefaultSong() {
        Song song = arrSongs.get(0);
        String filename = "android.resource://" + this.getPackageName() + "/raw/" + song.getUri();
        controller.playSong(filename);
    }


    @Override
    public void onStartSeek(int mediaDuration) {
        seekbar.setMax(mediaPlayer.getDuration());
        MainActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int mCurrentPosition = mediaPlayer.getCurrentPosition();
                    seekbar.setProgress(mCurrentPosition);
                }
                mHandler.postDelayed(this, 1000);
            }
        });
    }
}

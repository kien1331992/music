package com.kien.musiclistent.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kien.musiclistent.Database.annotation.Column;
import com.kien.musiclistent.Database.annotation.Ignore;
import com.kien.musiclistent.Database.annotation.ManyToOne;
import com.kien.musiclistent.Database.annotation.NotNull;
import com.kien.musiclistent.Database.annotation.OneToMany;
import com.kien.musiclistent.Database.annotation.OneToOne;
import com.kien.musiclistent.Database.annotation.PrimaryKey;
import com.kien.musiclistent.Database.annotation.Table;
import com.kien.musiclistent.Database.annotation.Unique;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kien on 4/9/18.
 */

public class SqliteHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "music.db";
    private static final int DB_VERSION = 1;
    private String DB_PATH = "";
    private Context context;
    private SQLiteDatabase db;

    public SqliteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        DB_PATH = this.context.getDatabasePath(DB_NAME).toString();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void createDataBase() {
        this.getWritableDatabase();
        copyDataBase();
    }

    public void copyDataBase() {
        try {
            InputStream myInput = context.getAssets().open(DB_NAME);
            String outFileName = DB_PATH;
            OutputStream myOutput = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    String getTableName(Class<?> table) {
        String name = table.getSimpleName();
        if (table.isAnnotationPresent(Table.class)) {
            name = table.getAnnotation(Table.class).name();
        }
        return name;
    }

    private String createTable(Class<?> table) {
        StringBuilder sb = new StringBuilder("CREATE TABLE ");
        sb.append(getTableName(table)).append("(");
        List<Field> fields = new ArrayList<Field>();
        Collections.addAll(fields, table.getDeclaredFields());
        while (table.getSuperclass() != null) {
            table = table.getSuperclass();
            Collections.addAll(fields, table.getDeclaredFields());
        }
        for (Field column : fields) {
            String columnName = column.getName();
            String columnType = getColumnType(column.getType());

            if (column.isAnnotationPresent(Ignore.class)
                    || column.isAnnotationPresent(OneToMany.class)
                    || Modifier.isTransient(column.getModifiers())
                    || Modifier.isStatic(column.getModifiers())
                    || Modifier.isVolatile(column.getModifiers())
                    || Modifier.isFinal(column.getModifiers())) continue;
            if (column.isAnnotationPresent(PrimaryKey.class)) {
                StringBuilder builder = new StringBuilder();
                PrimaryKey primaryKey = column.getAnnotation(PrimaryKey.class);
                builder.append(columnName).append(" ").append(columnType).append(" PRIMARY KEY");
                if (primaryKey.autoIncrease()) {
                    builder.append(" AUTOINCREMENT");
                }
                sb.insert(sb.indexOf("(") + 1, builder);
                continue;
            }

            if (columnType != null) {
                if (column.isAnnotationPresent(OneToOne.class)) {
                    OneToOne oneToOne = column.getAnnotation(OneToOne.class);
                    sb.append(", ").append(oneToOne.joinProperty()).append(" ").append(oneToOne
                            .typeOfJoin().getSimpleName());
                } else if (column.isAnnotationPresent(ManyToOne.class)) {
                    ManyToOne oneToOne = column.getAnnotation(ManyToOne.class);
                    sb.append(", ").append(oneToOne.joinProperty()).append(" ").append(oneToOne
                            .typeOfJoin().getSimpleName());
                } else if (column.isAnnotationPresent(Column.class)) {
                    Column columnAnnotation = column.getAnnotation(Column.class);
                    sb.append(", ").append(columnName).append(" ").append(columnType);

                    if (columnAnnotation.notNull()) {
                        if (columnType.endsWith(" NULL")) {
                            sb.delete(sb.length() - 5, sb.length());
                        }
                        sb.append(" NOT NULL");
                    }

                    if (columnAnnotation.unique()) {
                        sb.append(" UNIQUE");
                    }

                } else {
                    sb.append(", ").append(columnName).append(" ").append(columnType);

                    if (column.isAnnotationPresent(NotNull.class)) {
                        if (columnType.endsWith(" NULL")) {
                            sb.delete(sb.length() - 5, sb.length());
                        }
                        sb.append(" NOT NULL");
                    }

                    if (column.isAnnotationPresent(Unique.class)) {
                        sb.append(" UNIQUE");
                    }
                }
            }
        }
        sb.append(")");
        System.out.println(sb.toString());
        return sb.toString();
    }

    private String getColumnType(Class<?> type) {
        if ((type.equals(Boolean.class)) || (type.equals(Boolean.TYPE))
                || (type.equals(Integer.class)) || (type.equals(Integer.TYPE))
                || (type.equals(Long.class)) || (type.equals(Long.TYPE))) {
            return "INTEGER";
        }

        if ((type.equals(java.util.Date.class)) ||
                (type.equals(java.sql.Date.class)) ||
                (type.equals(java.util.Calendar.class))) {
            return "INTEGER NULL";
        }

        if (type.getName().equals("[B")) {
            return "BLOB";
        }

        if ((type.equals(Double.class)) || (type.equals(Double.TYPE))
                || (type.equals(Float.class)) || (type.equals(Float.TYPE))) {
            return "FLOAT";
        }

        if ((type.equals(String.class)) || (type.equals(Character.TYPE))) {
            return "TEXT";
        }

        return "";
    }
}

package com.kien.musiclistent.controller;

public interface MusicAction {
    void playSong(String resId);
    void stopSong();
    void nextSong();
    void reloadSong();
    void resume();
}

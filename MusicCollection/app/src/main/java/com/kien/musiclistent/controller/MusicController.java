package com.kien.musiclistent.controller;

import android.media.MediaPlayer;
import android.net.Uri;

import com.kien.musiclistent.MainActivity;

import java.io.IOException;

public class MusicController implements MusicAction {

    private MediaPlayer mediaPlayer;
    private MainActivity context;
    private int lengtStop;

    public MusicController(MainActivity ct) {
        this.context = ct;
    }

    public void setMediaPlayer(MediaPlayer _mediaPlayer, SeekBarDelegate _seekbar) {
        mediaPlayer = _mediaPlayer;
        seekBarDelegate = _seekbar;
    }


    @Override
    public void playSong(String path) {
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(context, Uri.parse(path));
            mediaPlayer.prepare();
            mediaPlayer.start();
            seekBarDelegate.onStartSeek(mediaPlayer.getDuration());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stopSong() {
        mediaPlayer.pause();
        lengtStop=mediaPlayer.getCurrentPosition();
    }

    @Override
    public void nextSong() {

    }

    @Override
    public void reloadSong() {

    }

    @Override
    public void resume() {
        mediaPlayer.seekTo(lengtStop);
        mediaPlayer.start();
    }

    public SeekBarDelegate seekBarDelegate;

    public interface SeekBarDelegate {
        void onStartSeek(int mediaDuration);
    }
}

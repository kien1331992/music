package com.kien.musiclistent.Database;

import android.content.Context;
import android.database.Cursor;

import com.kien.musiclistent.utils.Const;
import com.kien.musiclistent.utils.SharedPrefs;

import java.util.ArrayList;

public class SQLiteDAO {

    //read from folder assest
    public static void initialize(Context context) {
        SQLiteManager.initialize(context);
    }


    public static void createDB() {
        SQLiteManager.getInstance().createDB();
        SharedPrefs.getInstance().put(Const.EXITS_DB, true);
    }

    public static Boolean checkDB(String dbName) {
        return SQLiteManager.getInstance().isTableExists(dbName);
    }

    public static Boolean checkDB() {
        return SharedPrefs.getInstance().get(Const.EXITS_DB, Boolean.class);
    }

    public static long insertOrThrow(Object o) {
        return SQLiteManager.getInstance().insertOrThrow(o);
    }

    public static long insertOrUpdate(Object o) {
        return SQLiteManager.getInstance().insertOrUpdate(o);
    }

    public static void update(Object o) {
        SQLiteManager.getInstance().update(o);
    }

    public static <T> QueryBuilder<T> createQuery(Class<T> table) {
        return new QueryBuilder<>(table);
    }

    public static class QueryBuilder<T> {

        public ArrayList<T> perform() {
            SQLiteManager sqLiteManager = SQLiteManager.getInstance();
            Cursor cursor = sqLiteManager.performQuery(this);
            ArrayList<T> result = new ArrayList<>();
            try {
                T entity;
                while (cursor.moveToNext()) {
                    entity = (T) table.newInstance();
                    sqLiteManager.inflate(cursor, entity);
                    result.add(entity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
            return result;
        }

        Class table;
        StringBuilder where;
        String[] fieldSelected;
        //        String[] whereArgs;
        String orderBy;
        Integer limit;
        String groupBy;
        String having;

        private QueryBuilder(Class table) {
            this.table = table;
        }

        public QueryBuilder<T> where(String where) {
            if (this.where == null) this.where = new StringBuilder();
            this.where.append(where);
            return this;
        }

        public QueryBuilder<T> orderBy(String where) {
            if (this.where == null) this.where = new StringBuilder();
            this.where.append(where);
            return this;
        }

        public QueryBuilder<T> limit(int limit) {
            this.limit = limit;
            return this;
        }
    }
}

package com.kien.musiclistent.model;

import com.kien.musiclistent.Database.annotation.PrimaryKey;

import java.io.Serializable;

public class Song implements Serializable{
    @PrimaryKey
    private int id;

    private String name;
    private String uri;
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

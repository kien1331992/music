package com.kien.musiclistent.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.kien.musiclistent.Database.annotation.Ignore;
import com.kien.musiclistent.Database.annotation.ManyToOne;
import com.kien.musiclistent.Database.annotation.OneToMany;
import com.kien.musiclistent.Database.annotation.OneToOne;
import com.kien.musiclistent.Database.annotation.PrimaryKey;
import com.kien.musiclistent.Database.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

class SQLiteManager {
    private static SQLiteManager instance;
    private SqliteHelper helper;
    private SQLiteDatabase db;


    private SQLiteManager(Context context) {
        helper = new SqliteHelper(context);
        db = helper.getWritableDatabase();
    }

    public static void initialize(Context context) {
        instance = new SQLiteManager(context);
    }

    public boolean isTableExists(String tableName) {
        readDatabase();
        Cursor cursor = db.rawQuery("select name from sqlite_master where name =" +
                " '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public long insertOrThrow(Object object) {
        long id = -1;
        id = db.insertOrThrow(getTableName(object.getClass()), null, makeContentValue(object));
        List<Field> fields = getTableField(object.getClass());
        for (Field field : fields) {
            if (field.isAnnotationPresent(PrimaryKey.class) && field.getAnnotation(PrimaryKey
                    .class).autoIncrease()) {
                field.setAccessible(true);
                try {
                    field.set(object, id);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                } finally {
                    field.setAccessible(false);
                }
                break;
            }
        }
        return id;
    }

    public long insertOrUpdate(Object object) {
        try {
            return db.insertWithOnConflict(getTableName(object.getClass()), null,
                    makeContentValue(object), SQLiteDatabase.CONFLICT_REPLACE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void update(Object object) {
        List<Field> fields = getTableField(object.getClass());
        ContentValues values = new ContentValues();
        String whereClause = null;
        String[] whereArgs = null;
        for (Field column : fields) {
            if (column.isAnnotationPresent(OneToMany.class)) {
                continue;
            }
            if (column.isAnnotationPresent(PrimaryKey.class)) {
                whereClause = column.getName() + "=?";
                whereArgs = new String[]{getValue(object, column.getName()).toString()};
                continue;
            }
            String columnName = column.getName();
            Object obj = getValue(object, columnName);
            String value = (obj == null) ? (String) obj : obj.toString();
            if (column.isAnnotationPresent(OneToOne.class)) {
                OneToOne oneToOne = column.getAnnotation(OneToOne.class);
                columnName = oneToOne.joinProperty();
                if (obj != null) {
                    obj = getValue(obj, columnName);
                }
                value = (obj == null) ? (String) obj : obj.toString();
            }
            if (column.isAnnotationPresent(ManyToOne.class)) {
                ManyToOne manyToOne = column.getAnnotation(ManyToOne.class);
                columnName = manyToOne.joinProperty();
                if (obj != null) {
                    obj = getValue(obj, columnName);
                }
                value = (obj == null) ? (String) obj : obj.toString();
            }
            values.put(columnName, value);
        }
        try {
            db.update(getTableName(object.getClass()), values, whereClause, whereArgs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private ContentValues makeContentValue(Object object) {
        List<Field> fields = getTableField(object.getClass());
        ContentValues values = new ContentValues();
        for (Field column : fields) {
            if (column.isAnnotationPresent(OneToMany.class)) {
                continue;
            }
            if (column.isAnnotationPresent(PrimaryKey.class)) {
                if (column.getAnnotation(PrimaryKey.class).autoIncrease())
                    continue;
            }
            String columnName = column.getName();
            Object obj = getValue(object, columnName);
            String value = (obj == null) ? (String) obj : obj.toString();
            if (column.isAnnotationPresent(OneToOne.class)) {
                OneToOne oneToOne = column.getAnnotation(OneToOne.class);
                columnName = oneToOne.joinProperty();
                if (obj != null) {
                    obj = getValue(obj, columnName);
                }
                value = (obj == null) ? (String) obj : obj.toString();
            }
            if (column.isAnnotationPresent(ManyToOne.class)) {
                ManyToOne manyToOne = column.getAnnotation(ManyToOne.class);
                columnName = manyToOne.joinProperty();
                if (obj != null) {
                    obj = getValue(obj, columnName);
                }
                value = (obj == null) ? (String) obj : obj.toString();
            }
            values.put(columnName, value);
        }
        return values;
    }

    private void setFieldValueFromCursor(Cursor cursor, Field field, Object object) {
        field.setAccessible(true);
        try {
            Class<?> fieldType = field.getType();
            String colName = field.getName();
            if (field.isAnnotationPresent(OneToOne.class)) {
                colName = field.getAnnotation(OneToOne.class).joinProperty();
            }
            int columnIndex = cursor.getColumnIndex(colName);
            if (cursor.isNull(columnIndex)) {
                return;
            }
            if (field.isAnnotationPresent(OneToOne.class)) {
                Object o = findById(field.getType(), cursor.getInt(columnIndex));
                field.set(object, o);
            } else if (fieldType.equals(int.class) || fieldType.equals(Integer.class)) {
                field.set(object, cursor.getInt(columnIndex));
            } else if (fieldType.equals(long.class) || fieldType.equals(Long.class)) {
                field.set(object, cursor.getLong(columnIndex));
            } else if (fieldType.equals(String.class)) {
                String val = cursor.getString(columnIndex);
                field.set(object, val != null && val.equals("null") ? null : val);
            } else if (fieldType.equals(double.class) || fieldType.equals(Double.class)) {
                field.set(object, cursor.getDouble(columnIndex));
            } else if (fieldType.equals(boolean.class) || fieldType.equals(Boolean.class)) {
                field.set(object, cursor.getString(columnIndex).equals("1"));
            } else if (field.getType().getName().equals("[B")) {
                field.set(object, cursor.getBlob(columnIndex));
            } else if (fieldType.equals(float.class) || fieldType.equals(Float.class)) {
                field.set(object, cursor.getFloat(columnIndex));
            } else if (fieldType.equals(short.class) || fieldType.equals(Short.class)) {
                field.set(object, cursor.getShort(columnIndex));
            } else if (fieldType.equals(Timestamp.class)) {
                long l = cursor.getLong(columnIndex);
                field.set(object, new Timestamp(l));
            } else if (fieldType.equals(Date.class)) {
                long l = cursor.getLong(columnIndex);
                field.set(object, new Date(l));
            } else if (fieldType.equals(Calendar.class)) {
                long l = cursor.getLong(columnIndex);
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(l);
                field.set(object, c);
            } else if (Enum.class.isAssignableFrom(fieldType)) {
                try {
                    Method valueOf = field.getType().getMethod("valueOf", String.class);
                    String strVal = cursor.getString(columnIndex);
                    Object enumVal = valueOf.invoke(field.getType(), strVal);
                    field.set(object, enumVal);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            field.setAccessible(false);
        }
    }

    void inflate(Cursor cursor, Object object) {
        List<Field> fields = getTableField(object.getClass());
        String id = "";
        for (Field field : fields) {
            if (field.isAnnotationPresent(ManyToOne.class)) {
                continue;
            }
            setFieldValueFromCursor(cursor, field, object);
            if (field.isAnnotationPresent(PrimaryKey.class)) {
                id = String.valueOf(getValue(object, field.getName()));
            }
        }
        for (Field field : fields) {
            if (field.isAnnotationPresent(OneToMany.class)) {
                OneToMany oneToMany = field.getAnnotation(OneToMany.class);
                List toMany = search(oneToMany.referenceTable(), oneToMany.joinProperty() + "=?",
                        id);
                for (int i = 0; i < toMany.size(); i++) {
                    Object o = toMany.get(i);
                    List<Field> fieldList = new ArrayList<Field>();
                    Collections.addAll(fieldList, o.getClass().getDeclaredFields());
                    for (Field f : fieldList) {
                        if (f.isAnnotationPresent(ManyToOne.class)) {
                            f.setAccessible(true);
                            try {
                                f.set(o, object);
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            } finally {
                                f.setAccessible(false);
                            }
                            break;
                        }
                    }
                }
                field.setAccessible(true);
                try {
                    field.set(object, toMany);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                } finally {
                    field.setAccessible(false);
                }
            }
        }
    }

    String getTableName(Class<?> table) {
        String name = table.getSimpleName();
        if (table.isAnnotationPresent(Table.class)) {
            name = table.getAnnotation(Table.class).name();
        }
        return name;
    }

    public <T> T findById(Class<T> type, int id) {
        List<Field> fields = getTableField(type);
        String whereClause = null;
        String[] whereArgs = null;
        for (Field column : fields) {
            if (column.isAnnotationPresent(PrimaryKey.class)) {
                whereClause = column.getName() + "=?";
                whereArgs = new String[]{String.valueOf(id)};
                break;
            }
        }
        List<T> array = search(type, whereClause, whereArgs);
        if (array.size() > 0) return array.get(0);
        return null;
    }

    public <T> ArrayList<T> search(Class<T> type, String whereClause, String... whereArgs) {
        Cursor cursor = db.query(getTableName(type), null, whereClause, whereArgs, null, null,
                null, null);
        ArrayList<T> result = new ArrayList<T>();
        T entity;
        try {
            while (cursor.moveToNext()) {
                entity = type.newInstance();
                inflate(cursor, entity);
                result.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return result;
    }

    private Object getValue(Object obj, String columnName) {
        String first = columnName.substring(0, 1).toUpperCase(Locale.US);
        columnName = first + columnName.substring(1);
        Class table = obj.getClass();
        List<Method> methods = new ArrayList<Method>();
        Collections.addAll(methods, table.getDeclaredMethods());
        while (table.getSuperclass() != null) {
            table = table.getSuperclass();
            Collections.addAll(methods, table.getDeclaredMethods());
        }
        for (Method method : methods) {
            String name = method.getName();
            if (name.equals("get" + columnName) || name.equals("is" + columnName)) {
                try {
                    return method.invoke(obj, new Object[]{});
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private List<Field> getTableField(Class<?> table) {
        List<Field> fields = new ArrayList<Field>();
        Collections.addAll(fields, table.getDeclaredFields());
        while (table.getSuperclass() != null) {
            table = table.getSuperclass();
            Collections.addAll(fields, table.getDeclaredFields());
        }
        for (int i = fields.size() - 1; i >= 0; i--) {
            Field column = fields.get(i);
            if (column.isAnnotationPresent(Ignore.class)
                    || Modifier.isTransient(column.getModifiers())
                    || Modifier.isStatic(column.getModifiers())
                    || Modifier.isVolatile(column.getModifiers())
                    || Modifier.isFinal(column.getModifiers())) fields.remove(i);
        }
        return fields;
    }

    static SQLiteManager getInstance() {
        if (instance == null)
            throw new NullPointerException("SQLiteManager not initialized. Please, invoke " +
                    "initialize() method first");
        return instance;
    }

    public void createDB() {
        if (helper != null) {
            helper.createDataBase();
        }
    }

    private void readDatabase() {
        if (db == null || !db.isOpen()) {
            db = helper.getReadableDatabase();
        }

        if (!db.isReadOnly()) {
            db.close();
            db = helper.getReadableDatabase();
        }
    }

    Cursor performQuery(SQLiteDAO.QueryBuilder builder) {
        return db.query(
                getTableName(builder.table),
                builder.fieldSelected,
                builder.where == null ? null : builder.where.toString(),
                null,
                builder.groupBy,
                builder.having,
                builder.orderBy,
                builder.limit == null ? null : builder.limit.toString());
    }




}

package com.kien.musiclistent.Database.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by macmobiles on 8/1/2016.
*/
@Retention(RetentionPolicy.RUNTIME)
public @interface ManyToOne {
    String joinProperty();
    Class typeOfJoin();
}
